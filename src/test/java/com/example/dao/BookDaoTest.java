package com.example.dao;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.domain.Book;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class BookDaoTest {
  @Autowired
  private BookDao bookDao;

  @Test
  void testGetById() {

    System.out.println(bookDao.selectById(1));
  }

  @Test
  void testSave(){
    Book book = new Book();
    book.setType("test1");
    book.setName("test1");
    book.setDescription("test1");
    bookDao.insert(book);
  }

  @Test
  void testUpdate(){
    Book book = new Book();
    book.setId(20);
    book.setType("test2");
    book.setName("test2");
    book.setDescription("test2");
    bookDao.updateById(book);
  }

  @Test
  void testDlete(){
    bookDao.deleteById(21);

  }

  @Test
  void testGetAll(){
    System.out.println(bookDao.selectList(null));
  }

  @Test
  void testGetPage(){
    IPage page = new Page(2,5);
    System.out.println(bookDao.selectPage(page,null).getRecords());
  }

  @Test
  void testGetBy(){
    QueryWrapper<Book> qw = new QueryWrapper<>();
    qw.like("type","&");
    bookDao.selectList(qw);

  }

  @Test
  void testGetBy2(){
    String type = "&";
    LambdaQueryWrapper<Book> lqw = new LambdaQueryWrapper<>();
    lqw.like(type != null,Book::getType,type);
    bookDao.selectList(lqw);
  }
}
