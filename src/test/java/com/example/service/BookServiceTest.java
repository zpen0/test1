package com.example.service;

import com.baomidou.mybatisplus.core.metadata.IPage;import com.baomidou.mybatisplus.extension.plugins.pagination.Page;import com.example.domain.Book;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class BookServiceTest {
  @Autowired private IBookService bookService ;

  @Test
  void testGetById() {
    System.out.println(bookService.getById(4));
  }

  @Test
  void testUpdate() {
    Book book = new Book();
    book.setId(11);
    book.setType("test3");
    book.setName("test3");
    book.setDescription("test3");
    bookService.updateById(book);
  }

  @Test
  void testDlete() {
    bookService.removeById(16);
  }

  @Test
  void testGetAll() {
    System.out.println(bookService.list());
  }

  @Test
  void testSave(){
    Book book = new Book();
    book.setType("test4");
    book.setName("test4");
    book.setDescription("test4");
    bookService.save(book);
  }

  @Test
  void testGetPage(){
    IPage<Book> page = new Page<Book>(2,5);
    IPage<Book> page2 = bookService.page(page);
    System.out.println(page2);
    System.out.println(page2.getPages());
    System.out.println(page2.getRecords());
    System.out.println(page2.getSize());
    System.out.println(page2.getTotal());
    System.out.println(page2.getCurrent());
  }
}
