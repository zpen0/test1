package com.example.controller;

import com.alibaba.excel.EasyExcel;import com.alibaba.excel.support.ExcelTypeEnum;import com.example.domain.BookDo;import com.example.service.BookDoService;import com.fasterxml.jackson.core.type.TypeReference;import com.fasterxml.jackson.databind.ObjectMapper;import org.springframework.beans.factory.annotation.Autowired;import org.springframework.core.io.ClassPathResource;import org.springframework.web.bind.annotation.GetMapping;import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;import javax.servlet.http.HttpServletResponse;import java.io.IOException;import java.io.InputStream;import java.io.UnsupportedEncodingException;import java.net.URLEncoder;import java.util.List;

@RestController
@RequestMapping("/excel")
public class ExcelController {
    @Autowired
    private BookDoService bookDoService;

    @GetMapping("/export/book")
    public void exportBookExcel(HttpServletResponse response){
        try{
            this.setExcelResponseProp(response,"图书列表");
            List<BookDo> bookDoList = bookDoService.getBookList();
            EasyExcel.write(response.getOutputStream())
                    .head(BookDo.class)
                    .excelType(ExcelTypeEnum.XLSX)
                    .sheet("图书列表")
                    .doWrite(bookDoList);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    /**
     * 设置响应结果
     *
     * @param response    响应结果对象
     * @param rawFileName 文件名
     * @throws UnsupportedEncodingException 不支持编码异常
     */
    private void setExcelResponseProp(HttpServletResponse response, String rawFileName) throws UnsupportedEncodingException {
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        String fileName = URLEncoder.encode(rawFileName,"UTF-8");
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
    }



}
