package com.example.service.impl;

import com.example.domain.BookDo;import com.example.service.BookDoService;
import org.springframework.stereotype.Service;import java.util.ArrayList;import java.util.List;

@Service
public class BookDoServiceImpl implements BookDoService {
    @Override
    public List<BookDo> getBookList() {
        List<BookDo> book = new ArrayList<>();
        BookDo bookDo1 = new BookDo();
        bookDo1.setId(10000);
        bookDo1.setName("活着");
        bookDo1.setAuthor("余华");
        book.add(bookDo1);
        return book;
    }
}
