package com.example.service;

import com.example.domain.BookDo;import java.util.List;


public interface BookDoService {
    List<BookDo> getBookList();
}
