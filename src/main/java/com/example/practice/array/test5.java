package com.example.practice.array;

import java.util.Random;

public class test5 {
    public static void main(String[] args) {
        // 随机生成10个1-100的数字存入数组
        // 1.求和
        // 2.求平均数
        // 3.统计比平均数低的个数
        int [] arr = new int[10];
        int sum = 0;
        double average = 0;
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            Random r = new Random();
            int number = r.nextInt(100) + 1;
            arr[i] = number;
            System.out.println(arr[i]);
        }
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];

        }
        System.out.println("数组所有数据的和为：" + sum);
        average = sum / arr.length;
        System.out.println("数组所有数据的平均数为：" + average);
        for (int i = 0; i < arr.length; i++) {
            if(arr[i] < average) {
                count++;
            }
        }
        System.out.println("数组中比平均数低的个数为：" + count);
    }
}
