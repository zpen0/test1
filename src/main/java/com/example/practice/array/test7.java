package com.example.practice.array;

import java.util.Random;

public class test7 {
    public static void main(String[] args) {
        int [] arr = {1,2,3,4,5};
        int temp = 0;
        for (int i = 0; i < arr.length; i++) {
            Random r = new Random();
            int index = r.nextInt(arr.length );
            temp = arr[i];
            arr[i] = arr[index];
            arr[index] = temp;

        }
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }
}
