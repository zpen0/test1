package com.example.practice.array;

public class test4 {
    public static void main(String[] args) {
        // 求最大值
        int [] arr = {33,5,22,44,55};
        int max = 0;
        for (int i = 0; i < arr.length; i++) {
            if(arr[i] >= max) {
                max = arr[i];
            }
        }
        System.out.println(max);
    }
}
