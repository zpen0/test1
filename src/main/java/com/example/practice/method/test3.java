package com.example.practice.method;

public class test3 {
    public static void main(String[] args) {
        int [] arr = {1,2,55,33,534,75};
        int number = 33;
        boolean flag = judge(arr,number);
        if(flag){
            System.out.println("存在");
        }else {
            System.out.println("不存在");
        }
    }
    // 判断某个数是否存在
    public static boolean judge(int [] arr,int number) {

        for (int i = 0; i < arr.length; i++) {
            if(arr[i] == number) {
               return true;
            }
        }
        return false;
    }
}
