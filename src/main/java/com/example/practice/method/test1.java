package com.example.practice.method;

public class test1 {
    public static void main(String[] args) {
        // 第一季度
        double quarter1 = getQuarter(12.3,33.5,55.8);
        // 第二季度
        double quarter2 = getQuarter(11.6,39.5,22.6);
        // 第三季度
        double quarter3 = getQuarter(33.7,32.3,27.5);
        // 第四季度
        double quarter4 = getQuarter(11.8,21.6,28.3);
        // 年销售额
        double year = quarter1 + quarter2 +quarter3 +quarter4;
        System.out.println(year);
    }
    // 季度销售额
    public static double getQuarter(double month1,double month2,double month3) {
        double quarter = month1 + month2 +month3;
        return quarter;
    }
}
