package com.example.practice.method;

public class test2 {
    public static void main(String[] args) {
        int [] arr = {233,111,55,7777,4444,2242};
        int max = getMax(arr);
        System.out.println(max);
    }
    // 定义方法 求数组最大值
    public static int getMax(int [] arr) {
        int max = 0;
        for (int i = 0; i < arr.length; i++) {
            if(arr[i] >= max) {
                max = arr[i];
            }
        }
        return max;
    }
}
