package com.example.practice.method;

public class test4 {
    // 定义一个方法 copyOfRange(int [] arr,int from,int to)
    // 将数组arr从索引from（包含）开始
    // 到索引to（不包含）结束的元素复制到新数组
    // 将新数组返回
    public static void main(String[] args) {
        int [] arr = {1,2,3,4,5,6,7,8,9,0};
        int from = 2;
        int to = 9;
        int [] newArr = copyOfRange(arr,from,to);
        for (int i = 0; i < newArr.length; i++) {
            if(i == 0) {
                System.out.print("[" + newArr[i] + ",");
            } else if(i == newArr.length - 1) {
                System.out.print(newArr[i] + "]");
            } else {
                System.out.print(newArr[i] + ",");
            }

        }
    }
    public static int[] copyOfRange(int [] arr,int from,int to) {
        int [] newArr = new int[to - from];
        int index = 0;
        for (int i = from; i < to; i++) {
            newArr[index] = arr[i];
            index++;
        }
        return newArr;
    }
}
