package com.example.practice.interfaceTest;

public class PingPangSporter extends Sporter implements English{
    public PingPangSporter() {
    }

    public PingPangSporter(String name, int age) {
        super(name, age);
    }

    @Override
    public void speakEnglish(){
        System.out.println("乒乓球运动员在说英语");
    };
    @Override
    public void study(){
        System.out.println("乒乓球运动员学乒乓球");
    }

}
