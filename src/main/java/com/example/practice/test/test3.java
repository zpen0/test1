package com.example.practice.test;

import java.util.Random;

public class test3 {
//     需求：
//     在歌唱比赛中，有6名评委给选手打分，分数范围是[0-100]之间的整数。
//     选手的最后的得分为：去掉最高分、最低分后的四个评委的平均分。
    public static void main(String[] args) {
        double average = getAverage();
        System.out.println("平均数为：" + average);
    }
    public static  double getAverage(){
        // 1.将六次打分存入数组
        int [] scores = new int[6];
        for (int i = 0; i < 6; i++) {
            Random r = new Random();
            int score = r.nextInt(100);
            scores[i] = score;
            System.out.print(scores[i] + " " );
        }

        // 2.获取最大值和最小值并求和
        int max = scores[1];
        int min = scores[1];
        int sum = 0;
        double average = 0;
        for (int i = 0; i < scores.length; i++) {
            if(scores[i] >= max) {
                max = scores[i];
            }
            if(scores[i] <= min) {
                min = scores[i];
            }
            sum += scores[i];

        }
        System.out.println();
        System.out.println("和为：" + sum);
        System.out.println("最大值是：" + max);
        System.out.println("最小值是:" + min);

        // 3.去掉最大值和最小值求平均数
        average = (sum - max - min) / (scores.length - 2);

        return average;
    }
}
