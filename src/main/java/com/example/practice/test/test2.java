package com.example.practice.test;

import java.util.Random;

public class test2 {
    // 需求：定义方法实现随机产生一个5位的验证码
    // 验证码格式：
    // 1.长度为5
    // 2.前四位是大写字母或者小写字母
    // 3.最后一位是数字
    public static void main(String[] args) {
        String verficationCode = getVerificationCode();
        System.out.println(verficationCode);
    }
    public static String getVerificationCode() {
        // 1.将大小写字母存入数组
        char [] arr1 = new char[52];
        int [] arr2 = {1,2,3,4,5,6,7,8,9,0};
        String verificationCode = "";
        for (int i = 0; i < arr1.length; i++) {
            if(i <= 25) {
                arr1[i] = (char)(97 + i);
            } else {
                arr1[i] = (char)(65 + i - 26);
            }
        }

        // 2.随机取四位大小写字母
        for (int i = 0; i < 4; i++) {
            Random r = new Random();
            int num = r.nextInt(arr1.length);
            verificationCode += arr1[num];
        }

        //3.取一位数字
        Random r = new Random();
        int num = r.nextInt(arr2.length);
        verificationCode += arr2[num];

        return verificationCode;
    }
}
