package com.example.practice.test;

import java.util.Scanner;

public class test1 {
    // 需求：
    // 1.机票价格按照单机旺季、头的给舱和经济舱收费、输入机票原价、月份和头等舱或经济舱。
    // 2.按照如下规则计算机票价格：旺季（5-10月）头等舱9折，经济舱8.5折，淡季（11月到来年4月）头等舱7折，经济舱6.5折
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("请输入机票原价：");
        double price = sc.nextDouble();
        System.out.print("请输入月份：");
        int month = sc.nextInt();
        System.out.print("请输入0(头等舱)或1(经济舱)：");
        int type = sc.nextInt();
        price = getPrice(price,month,type);
        if(price == 0) {
            System.out.println("输入不合法");
        } else {
            System.out.print("您的机票价格为：" + price);
        }

    }
    public static double getPrice(double price,int month,int type) {
        switch (type) {
            case 0:
                if(month >=5 && month <= 10) {
                    price *= 0.9;
                } else if (month >=1 && month <=4 || month >= 11 && month <= 12 ) {
                    price *= 0.7;
                } else {
                    price = 0;
                }
                break;
            case 1 :
                if(month >=5 && month <= 10) {
                    price *= 0.85;
                } else if (month >=1 && month <=4 || month >= 11 && month <= 12 ) {
                    price *= 0.65;
                } else {
                    price = 0;
                }
                break;
            default:
                price = 0;
        }
        return price;
    }
}
