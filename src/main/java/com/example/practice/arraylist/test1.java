package com.example.practice.arraylist;

import java.util.ArrayList;

public class test1 {
    // 定义一个集合，添加一些学生对象，并进行遍历
    public static void main(String[] args) {
        ArrayList<Student> list = new ArrayList<>();

        // 创建学生对象
        Student s1 = new Student("张三",19);
        Student s2 = new Student("李四",18);
        Student s3 = new Student("王五",20);

        // 向集合里添加元素
        list.add(s1);
        list.add(s2);
        list.add(s3);

        // 遍历
        for (int i = 0; i < list.size(); i++) {
            Student stu = list.get(i);
            System.out.println(stu.getName() + stu.getAge());
        }

    }
}
