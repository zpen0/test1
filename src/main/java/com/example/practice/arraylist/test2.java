package com.example.practice.arraylist;

import java.util.ArrayList;
import java.util.Scanner;

public class test2 {
    // 定义方法根据id查找对应的用户信息
    public static void main(String[] args) {
        ArrayList<User> list = new ArrayList<>();

        User user1 = new User(1000,"zpen1","123456");
        User user2 = new User(1001,"zpen2","123456");
        User user3 = new User(1002,"zpen3","123456");

        list.add(user1);
        list.add(user2);
        list.add(user3);

        Scanner sc = new Scanner(System.in);
        System.out.println("请输入要查找的id:");
        int id = sc.nextInt();

        boolean flag = contains(list,id);
        if(flag){
            User user = searchUser(list,id);
            System.out.println(user.getId() + " " + user.getUsername() + " " + user.getPassword());
        } else {
            System.out.println("用户不存在");
        }

    }

    public static boolean contains(ArrayList<User> list,int id){
        for (int i = 0; i < list.size(); i++) {
            if(list.get(i).getId() == id) {
                return true;
            }
        }
        return false;
    }

    public static User searchUser(ArrayList<User> list,int id) {
        int index = 0;
        for (int i = 0; i < list.size(); i++) {
            if(list.get(i).getId() == id) {
                index = i;
            }
        }
        return list.get(index);
    }
}
