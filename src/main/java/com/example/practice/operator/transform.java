package com.example.practice.operator;

public class transform {
    public static void main(String[] args) {
        // 隐式转换 小转大
        // byte < short < int < long < float < double
        // byte、short、char 先转化成int再计算
        int a = 10;
        double b = a;
        byte c = 10;
        char d = 20;
        System.out.println(b);
        System.out.println(c + d);

        // 强制转换
        double e = 20.0;
        int f = (int) (e + d);
        System.out.println(e + d);
        System.out.println(f);
        System.out.println(4 | 3);
    }
}
