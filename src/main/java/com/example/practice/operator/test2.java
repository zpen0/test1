package com.example.practice.operator;

import java.util.Scanner;

public class test2 {
    public static void main(String[] args) {
        // 键盘录入两个整数，如果其中一个数为6或者两个数的和是6的倍数输出true，否则false
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入第一个整数：");
        int number1 = sc.nextInt();
        System.out.println("请输入第二个整数：");
        int number2 = sc.nextInt();
        boolean result = number1 == 6 || number2 == 6 || (number1 + number2) % 6 == 0;
        System.out.println(result);


    }
}
