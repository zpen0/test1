package com.example.practice.object.fight;

public class RoleTest {
    public static void main(String[] args) {
        // 创建第一个角色
        Role r1 = new Role("王刚",100);
        // 创建第二个角色
        Role r2 = new Role("丁林波",100);

        // 开始格斗
        while (true) {
            r1.attack(r2);
            if(r2.getBlood() == 0) {
                System.out.println(r1.getName() + "KO了" + r2.getName());
                break;
            }

            r2.attack(r1);
            if(r1.getBlood() == 0) {
                System.out.println(r2.getName() + "KO了" + r1.getName());
                break;
            }
        }
    }
}
