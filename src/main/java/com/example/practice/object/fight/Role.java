package com.example.practice.object.fight;

import java.util.Random;

public class Role {
    private String name;
    private int blood;

    public Role(){

    }

    public Role(String name,int blood) {
        this.name = name;
        this.blood = blood;
    }

    public String getName() {
        return name;
    }

    public  void setName(String name) {
        this.name = name;
    }

    public int getBlood() {
        return blood;
    }

    public void setBlood(int blood) {
        this.blood = blood;
    }

    // 攻击
    // 方法调用者攻击参数
    public void attack(Role role){
        // 造成的伤害
        Random r = new Random();
        int harm = r.nextInt(20);
        // 剩余血量
        int remainBlood = role.getBlood() - harm;
        remainBlood =  remainBlood>=0?remainBlood:0;
        role.setBlood(remainBlood);

        System.out.println(this.getName() + "攻击" + role.getName() + "造成" + harm + "伤害，" + role.getName() + "还剩" + remainBlood + "血量");
    }
}
