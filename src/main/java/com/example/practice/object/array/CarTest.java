package com.example.practice.object.array;

import java.util.Scanner;

public class CarTest {
    public static void main(String[] args) {
        Car [] cars = new Car[3];
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < cars.length; i++) {
            Car car = new Car();
            System.out.println();
            System.out.print("请输入汽车的品牌：");
            String brand = sc.next();
            car.setBrand(brand);
            System.out.print("请输入汽车的价格：");
            double price = sc.nextDouble();
            car.setPrice(price);
            System.out.print("请输入汽车的颜色：");
            String color = sc.next();
            car.setColor(color);
            cars[i] = car;

        }
        System.out.println();
        System.out.println("品牌 " + "价格 " + "颜色");
        for (int i = 0; i < cars.length; i++) {
            System.out.println(cars[i].getBrand() +" " + cars[i].getPrice() + " " +cars[i].getColor());
        }

    }

}
