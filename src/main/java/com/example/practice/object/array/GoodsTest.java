package com.example.practice.object.array;

public class GoodsTest {
    public static void main(String[] args) {
        Goods good1 = new Goods(1,"巧克力",15,9000);
        Goods good2 = new Goods(2,"牛奶",4.5,1200);
        Goods good3 = new Goods(3,"曲奇饼干",10,10000);

        Goods [] goods = new Goods[3];

        goods[0] = good1;
        goods[1] = good2;
        goods[2] = good3;

        System.out.println("商品信息：");
        System.out.println();
        for (int i = 0; i < goods.length; i++) {

            System.out.println("id：" + goods[i].getId());
            System.out.println("名称: " + goods[i].getName());
            System.out.println("价格: " + goods[i].getPrice());
            System.out.println("库存: " + goods[i].getStock());
            System.out.println();

        }

    }

}
