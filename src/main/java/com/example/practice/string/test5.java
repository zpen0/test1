package com.example.practice.string;

public class test5 {
    // 反转字符串
    public static void main(String[] args) {
        String str = "malegeb";
        String newStr = reverseString(str);
        System.out.println(newStr);
    }
    public  static String reverseString(String str) {
        String newStr= "";
        for (int i = str.length() - 1; i >= 0; i--) {
            char c = str.charAt(i);
            newStr = newStr + c;
        }
        return  newStr;
    }
}
