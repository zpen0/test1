package com.example.practice.string;

public class test4 {
    // 定义方法拼接字符串
    public static void main(String[] args) {
        int [] arr = {1,2,3,4,5};
        String str = arrToString(arr);
        System.out.println(str);
    }
    public static String arrToString(int [] arr) {
        String str = "[";
        for (int i = 0; i < arr.length; i++) {
            if(i == arr.length - 1) {
                str = str + arr[i] + ']';
            } else {
                str = str + arr[i] + ',';
            }

        }
        return str;
    }
}
