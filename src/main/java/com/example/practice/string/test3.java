package com.example.practice.string;

public class test3 {
    // 统计字符串中大写字母、小写字母、数字出现的次数
    public static void main(String[] args) {
        String str = "ab123CDE";
        int bigCount = 0;
        int smallCount = 0;
        int numCount = 0;
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if(c >= 'a' && c <= 'z') {
                smallCount++;

            }
            if(c >= 'A' && c <= 'Z') {
                bigCount++;
            }

            if(c >= '0' && c <= '9') {
                numCount++;
            }

        }
        System.out.println(str);
        System.out.println("大写字母的数量为：" + bigCount );
        System.out.println("小写字母的数量为：" + smallCount );
        System.out.println("数字的数量为：" + numCount );
    }
}
