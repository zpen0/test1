package com.example.demo3.domain;

public class Course {
  private int courseID;
  private String courseName;
  private int teacherID;
  private String courseTime;
  private String classRoom;
  private int courseWeek;
  private String courseType;
  private int collegeID;
  private int score;

  public Course() {}

  public Course(
      int courseID,
      String courseName,
      int teacherID,
      String courseTime,
      String classRoom,
      int courseWeek,
      String courseType,
      int collegeID,
      int score) {
    this.courseID = courseID;
    this.courseName = courseName;
    this.teacherID = teacherID;
    this.courseTime = courseTime;
    this.classRoom = classRoom;
    this.courseWeek = courseWeek;
    this.courseType = courseType;
    this.collegeID = collegeID;
    this.score = score;
  }
  /**
   * 获取
   *
   * @return courseID
   */

  public int getCourseID() {
    return courseID;
  }
  /**
   * 设置
   *
   * @param courseID
   */

  public void setCourseID(int courseID) {
    this.courseID = courseID;
  }
  /**
   * 获取
   *
   * @return courseName
   */

  public String getCourseName() {
    return courseName;
  }
  /**
   * 设置
   *
   * @param courseName
   */

  public void setCourseName(String courseName) {
    this.courseName = courseName;
  }
  /**
   * 获取
   *
   * @return teacherID
   */

  public int getTeacherID() {
    return teacherID;
  }
  /**
   * 设置
   *
   * @param teacherID
   */

  public void setTeacherID(int teacherID) {
    this.teacherID = teacherID;
  }
  /**
   * 获取
   *
   * @return courseTime
   */

  public String getCourseTime() {
    return courseTime;
  }
  /**
   * 设置
   *
   * @param courseTime
   */

  public void setCourseTime(String courseTime) {
    this.courseTime = courseTime;
  }
  /**
   * 获取
   *
   * @return classRoom
   */

  public String getClassRoom() {
    return classRoom;
  }
  /**
   * 设置
   *
   * @param classRoom
   */

  public void setClassRoom(String classRoom) {
    this.classRoom = classRoom;
  }
  /**
   * 获取
   *
   * @return courseWeek
   */

  public int getCourseWeek() {
    return courseWeek;
  }
  /**
   * 设置
   *
   * @param courseWeek
   */

  public void setCourseWeek(int courseWeek) {
    this.courseWeek = courseWeek;
  }
  /**
   * 获取
   *
   * @return courseType
   */

  public String getCourseType() {
    return courseType;
  }
  /**
   * 设置
   *
   * @param courseType
   */

  public void setCourseType(String courseType) {
    this.courseType = courseType;
  }
  /**
   * 获取
   *
   * @return collegeID
   */

  public int getCollegeID() {
    return collegeID;
  }
  /**
   * 设置
   *
   * @param collegeID
   */

  public void setCollegeID(int collegeID) {
    this.collegeID = collegeID;
  }
  /**
   * 获取
   *
   * @return score
   */

  public int getScore() {
    return score;
  }
  /**
   * 设置
   *
   * @param score
   */

  public void setScore(int score) {
    this.score = score;
  }

  @Override
  public String toString() {
    return "Course{courseID = "
        + courseID
        + ", courseName = "
        + courseName
        + ", teacherID = "
        + teacherID
        + ", courseTime = "
        + courseTime
        + ", classRoom = "
        + classRoom
        + ", courseWeek = "
        + courseWeek
        + ", courseType = "
        + courseType
        + ", collegeID = "
        + collegeID
        + ", score = "
        + score
        + "}";
  }
}
