package com.example.demo3.dao;

import com.example.demo3.domain.Course;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface CourseDao {
  @Select("select * from course where courseID = #{id}")
  public Course getByID(int id);
}
