package com.example.domain;

import lombok.Data;

@Data
public class User {
    private String name;
    private int line;
    private int commitTimes;
}
