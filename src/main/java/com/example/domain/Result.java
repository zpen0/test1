package com.example.domain;

import lombok.Data;

@Data
public class Result {
    private String username;
    private int line;
    private int commitTimes;
}
