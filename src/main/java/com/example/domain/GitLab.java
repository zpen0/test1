package com.example.domain;

import lombok.Data;

@Data
public class GitLab {
    private String url;
    private long id;
    private String token;
    private String branch;
    private String month;

}
