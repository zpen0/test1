package com.example.domain;

import com.alibaba.excel.annotation.ExcelProperty;import com.alibaba.excel.annotation.write.style.ColumnWidth;import lombok.Data;

@Data
public class BookDo {
    @ExcelProperty("图书编号")
    @ColumnWidth(20)
    private int id;

    @ExcelProperty("图书名")
    @ColumnWidth(20)
    private String name;

    @ExcelProperty("作者")
    @ColumnWidth(20)
    private String author;


}
