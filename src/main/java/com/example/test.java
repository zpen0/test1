package com.example;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import com.example.domain.GitLab;
import com.example.domain.Result;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Commit;
import org.springframework.stereotype.Service;
import java.util.*;


public class test {

    public static void main(String[] args) throws ParseException,GitLabApiException {
        GitLab gitLab1 = new GitLab();
        gitLab1.setId(45382432);
        gitLab1.setUrl("https://gitlab.com");
        gitLab1.setBranch("main");
        gitLab1.setToken("glpat-eCp7K_3TAn3yTTze-z-V");
        gitLab1.setMonth("2023-05");
        Set<String> username = new HashSet<>();
        Map<String, Result> map = new HashMap<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        GitLabApi gitLabApi =new GitLabApi(gitLab1.getUrl(),gitLab1.getToken());

        Date start = sdf.parse(gitLab1.getMonth() + "-01");
        Date end = sdf.parse(gitLab1.getMonth() + "-28");

        List<Commit> commits = gitLabApi.getCommitsApi().getCommits(gitLab1.getId(), "main", start,end);

        for(Commit commit : commits) {
            Result result = null;
            if (!username.contains(commit.getAuthorName())) {
                result = new Result();
                result.setUsername(commit.getAuthorName());
                result.setLine(0);
                result.setCommitTimes(0);
                map.put(commit.getAuthorName(), result);
                username.add(commit.getAuthorName());
            } else {
                result = map.get(commit.getAuthorName());


            }
            System.out.print(commit.getAuthorName() + sdf.format(commit.getCommittedDate()));
            commit = gitLabApi.getCommitsApi().getCommit(gitLab1.getId(), commit.getShortId());


            result.setLine(result.getLine() + commit.getStats().getTotal());
            result.setCommitTimes(result.getCommitTimes() + 1);

            System.out.println("变更行数:" + commit.getStats().getTotal() + ",累计变更行数：" + result.getLine() + ",累计提交次数：" + result.getCommitTimes());


        }
        System.out.println(map.values());
        String result = map.get("zpen0201").getLine() + "行，" + map.get("zpen0201").getCommitTimes() + "次";


    }
}
