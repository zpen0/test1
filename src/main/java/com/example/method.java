package com.example;

import com.example.domain.User;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Commit;
import java.util.*;

public class method {
  static long id = 45382432;

  public static void main(String[] args) throws ParseException, GitLabApiException {
    Set<String> username = new HashSet<>();
    Map<String, User> map = new HashMap<>();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    String token = "glpat-eCp7K_3TAn3yTTze-z-V";
    // 这写上你家gitlab地址
    GitLabApi gitLabApi = new GitLabApi("https://gitlab.com", token);

    // 起止时间
    Date start = sdf.parse("2023-5-01");
    Date end = sdf.parse("2023-5-28");
//    for (int number : OBJECT_NUMBERS) {
      List<Commit> commits = gitLabApi.getCommitsApi().getCommits(id, "main", start,end);

      for (Commit commit : commits) {
        User user = null;
        if (!username.contains(commit.getAuthorName())) {
          user = new User();
          user.setName(commit.getAuthorName());
          user.setLine(0);
          user.setCommitTimes(0);
          map.put(commit.getAuthorName(), user);
          username.add(commit.getAuthorName());
        } else {
          user = map.get(commit.getAuthorName());


        }
        System.out.print(commit.getAuthorName() + sdf.format(commit.getCommittedDate()));
        commit = gitLabApi.getCommitsApi().getCommit(id, commit.getShortId());


        user.setLine(user.getLine() + commit.getStats().getTotal());
        user.setCommitTimes(user.getCommitTimes() + 1);


        System.out.println(
            "变更行数:"
                + commit.getStats().getTotal()
                + ",累计变更行数："
                + user.getLine()
                + ",累计提交次数："
                + user.getCommitTimes());
      }
//    }

    System.out.println(map.values());
    System.out.println(map.get("zpen0201").getLine());
    System.out.println(map.get("zpen0201").getCommitTimes());
  }
}
